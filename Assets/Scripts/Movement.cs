using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Movement : MonoBehaviour
{
    private int value;
    public GameObject cubeta;
    public GameObject tortu;
    private float[] tortuxy;
    private float[] cubexy;
    
    // Start is called before the first frame update
    void Start()
    {
        value = 0;
        tortuxy = new float[] {1,1};
        cubexy = new float[] {1,1};
    }



    // Update is called once per frame
    void Update()
    {
        tortuxy[0] = Mathf.Round(tortu.transform.position.x);
        cubexy[0] = Mathf.Round(cubeta.transform.position.x);

        tortuxy[1] = Mathf.Round(tortu.transform.position.z);
        cubexy[1] = Mathf.Round(cubeta.transform.position.z);

        if(tortuxy.SequenceEqual(cubexy)){
            turttleMoves();
        }
    }

    void turttleMoves(){
        if(value==0){
        transform.Translate(0, Time.deltaTime, 0, Space.World);
            if(transform.position.y >= 0.6){
                value = 1;
            }
        } else {
            transform.Translate(0, -1 * Time.deltaTime, 0, Space.World);
            if(transform.position.y <= 0.55){
                value = 0;
            }
            }
    }
}
