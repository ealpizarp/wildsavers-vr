using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToDestination : MonoBehaviour
{

    public Transform waypoint;
    public string finalAnimation = null;
    UnityEngine.AI.NavMeshAgent agent;

    // Start is called before the first frame update
    void Start()
    {

    agent = GetComponent<UnityEngine.AI.NavMeshAgent>(); 
        
    }

    // Update is called once per frame
    void Update()
    {
    
    if (agent.remainingDistance <= 0.025 && !agent.pathPending) {
    Animator animator = GetComponent<Animator>();
    // animator.SetBool(finalAnimation, true);
    // Debug.Log("Reached destination!");
    }
    }

    public void moveToWaypoint() {
        agent.destination = waypoint.position; 
    }

//   bool CheckDestinationReached() {
//     float distanceToTarget = Vector3.Distance(transform.position, waypoint);
//     if(distanceToTarget < destinationReachedTreshold) { 
//     return true;
//     }
//     return false;
//     }
}
