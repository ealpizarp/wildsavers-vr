using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionObjects : MonoBehaviour
{
    public int nestFull;
    private List<GameObject> eggs;

    // Start is called before the first frame update
    void Start()
    {
        nestFull = 0;
        eggs = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnCollisionEnter(Collision collision)
    {
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (collision.gameObject.name == "Egg3" || collision.gameObject.name == "Egg2" || collision.gameObject.name == "Egg1")
        {
            itemExist(collision.gameObject);
        }
    }

    void itemExist(GameObject item){
         if (!(eggs.Contains(item))){
            eggs.Add(item);
            ends();
        }
    }

    void ends(){
        if(eggs.Count == 3){
            nestFull = 1;
            Debug.Log("All eggs are in the nest");
        }
    }

}
